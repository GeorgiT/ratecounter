import Vue from 'vue';
import Router from 'vue-router';
import PublicRoutes from './public.routes';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    ...PublicRoutes,
  ],
});
