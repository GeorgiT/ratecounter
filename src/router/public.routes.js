const Home = () => import(
    '@/views/public/home/Home'
    /* webpackChunkName: "Home" */
);
const Rates = () => import(
    '@/views/public/rates/Rates'
    /* webpackChunkName: "Home" */
);
const Top = () => import(
    '@/views/public/top/Top'
    /* webpackChunkName: "Home" */
);
// redirect: '/' while components are not ready.

export default [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/rates',
        name: 'rates',
        component: Rates
    },
    {
        path: '/top',
        name: 'top',
        component: Top
    },
];
