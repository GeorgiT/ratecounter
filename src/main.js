// NPM Packages
import Vue from 'vue';
import Buefy from 'buefy';
import VueFire from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'
import Notifications from 'vue-notification';
import router from './router';
import store from './store';
import App from './App.vue';
// CSS Libararies
import 'buefy/dist/buefy.css';
import 'vue-loading-overlay/dist/vue-loading.css';
import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/css/fontawesome.css";

Vue.use(Buefy, {
    defaultIconPack: "fas"
});
Vue.use(VueFire);
Vue.use(Notifications);

const config = {
    apiKey: 'AIzaSyDaueRc7lZrmkJFaUh_ZPh_VhmC-jlifYY',
    authDomain: 'ratecounter.firebaseapp.com',
    databaseURL: 'https://ratecounter.firebaseio.com',
    projectId: 'ratecounter',
    storageBucket: 'ratecounter.appspot.com',
    messagingSenderId: '433841695289'
};
let app = firebase.initializeApp(config);
export const db = firebase.firestore();

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
