import {http} from '@/axios';
import router from '@/router';
import { db } from '@/main';
// This module is meant to be loaded from the components that need it.

export default {
    namespaced: true,
    state() {
        return {
            
        };
    },
    getters: {

    },

    mutations: {

    },

    actions: {

    }
};
