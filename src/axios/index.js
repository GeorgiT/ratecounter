import Vue from 'vue';
import axios from 'axios';
import store from '@/store';

export const http = axios.create({
    baseURL: process.env.API_BASE_URL,
    headers: {
        common: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    }
});

const success = response => {
    if (response && process.env.NODE_ENV === 'development') {
        // Log endpoint before reponse object.
        console.log(`/${response.request.responseURL.split('/').slice(4).join('/')}`);
    }
    return response;
};

const error = (response) => {
    console.log(response);
    if (response && process.env.NODE_ENV === 'development') {
        console.log(`/${response.request.responseURL.split('/').slice(4).join('/')}`);
    }
    if (response.status === 401 && store.state.route.name !== 'login') {
        store.dispatch('session/logout');
    }
    return Promise.reject(response);
};

http.interceptors.response.use(success, error);

export function setHttpToken (token) {
    http.defaults.headers.common["Authorization"] = " Bearer " + token;
}

Vue.prototype.$http = http;
